import { useEffect, useState, useContext } from "react";
import { Button, Table, Container } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import ListOfOrders from '../components/ListOfOrders';
import UserContext from '../UserContext';

export default function ViewAllOrders() {
    const { user } = useContext(UserContext);
    const redirect = useNavigate();
    const [orders, setOrders] = useState([]); 
    
    useEffect(() => {
        fetch("https://warm-fjord-87149.herokuapp.com/users/viewAllOrders", {
             headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            // console.log(data);
            setOrders(data.map(order => {
                return (
                    <ListOfOrders key={order._id} orderProp={order} />
                )
            }))
        })
    }, []);

    return (
        (user.isAdmin === false || user.id === null) ?
            redirect('/')
        :
        <Container className='mx-5'>
            <Table bordered className="mt-5">
                <thead>
                    <tr>
                        <th>Customer Name</th>
                        <th>Product Id</th>
                        <th>Total Amount</th>
                        <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    { orders }
                </tbody>
            </Table>
            <Button as={Link} to="/admin/dashboard" className="mt-5" variant="outline-dark">
                BACK TO DASHBOARD
            </Button> 
        </Container>
    );
}
