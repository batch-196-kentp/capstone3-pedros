import { useState, useEffect, useContext } from 'react';
import { Table, Button, Container } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import ProductCreate from '../components/ProductCreate';
import ControlProducts from '../components/ControlProducts';
import UserContext from '../UserContext';

export default function DashBoard() {

    const {user, setUser} = useContext(UserContext);
    const redirect = useNavigate();

    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch('https://warm-fjord-87149.herokuapp.com/products', {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);

            const productList = (data.map(product => {
                return (
                    <ControlProducts key={product._id} productProp={product} />
                )
            }))
            setProducts(productList);
        });
    }, []);

    return (
        (user.isAdmin) ?
        <Container className='mx-5'>
            <h1 className="mt-5">Admin Panel</h1>
            
            <ProductCreate/> &nbsp;

            <Button as={Link} to="/admin/viewAllOrders" className="mt-5 btn btn-light border border-dark">
                View Orders
            </Button> 

            <Table bordered className="mt-5">
                <thead>
                    <tr>
                        <th>NAME</th>
                        <th>DESCRIPTION</th>
                        <th>PRICE</th>
                        <th>AVAILABLE</th>
                        <th >ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    { products }
                </tbody>
            </Table>
        </Container>
        :
        redirect('/home')
    )
}

