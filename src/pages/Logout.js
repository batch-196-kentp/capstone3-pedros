import {Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';

export default function Logout(){
	const {unsetUser, setUser} = useContext(UserContext);

		// clear the localStorage of the user's information
	unsetUser();
	useEffect(() => {

			// set the user state back to it's original state
			setUser({id: null})
		});

	return(
		<Navigate to="/login"/>

	)
};
