
import ProductView from '../components/ProductView';
import ProductCard from '../components/ProductCard'
import {Row} from 'react-bootstrap'
import { useEffect, useState, useContext } from "react";
import { useNavigate } from "react-router-dom";

import UserContext from '../UserContext';

export default function Products() {
	const { user } = useContext(UserContext);
    const redirect = useNavigate();
    const [products, setProducts] = useState([]);

	useEffect(()=>{
		fetch('https://warm-fjord-87149.herokuapp.com/products/active')
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			const productsArr = (data.map(product => {
				return (
					<ProductView key={product._id} productProp={product} breakpoint={4}/>
				)
			}))
			setProducts(productsArr)
		})
	},[products])

	return (
	    (user.isAdmin === true) ?
	        redirect('/')
	    :
	    <>
	        <h3 className="title-header mt-3 mx-5">Available Products</h3>
	           	<div className="grid">
	                {products}
	            </div>
	    </>
	)
}
