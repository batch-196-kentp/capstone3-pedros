import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import '../App.css';

export default function Home() {
	const data = {
        title: "Petro Solx",
        content: "We keep you fueled!",
        destination: "/products",
        label: "Order"
    }

    return (
        <>
	        <Highlights />

		</>
	)
}
