import { useEffect, useState } from 'react';
import { Container, Row, Col, ListGroup} from 'react-bootstrap';


export default function AdminProfile(){
    const [firstName , setFirstName] = useState('');
    const [lastName , setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo , setMobileNo] = useState('');

    useEffect(() => {
        fetch("https://warm-fjord-87149.herokuapp.com/users/details", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (response => response.json())
        .then(data => {
            // console.log(data._id);
            setFirstName(data.firstName);
            setLastName(data.lastName);
            setEmail(data.email);
            setMobileNo(data.mobileNo);
        })
    }, []);


    return (
        <Container className="mt-5 mx-5">
            <Row>
                <Col>
                    
                    <ListGroup className="profile-list-group" variant="flush">
                        <h3 className="title-header">Admin Details</h3>
                        <ListGroup.Item></ListGroup.Item>
                        <ListGroup.Item><p className='fw-bold mb-0'>Name:</p> {firstName} {lastName}</ListGroup.Item>
                        <ListGroup.Item><p className='fw-bold mb-0'>Email:</p> {email}</ListGroup.Item>
                        <ListGroup.Item><p className='fw-bold mb-0'>Mobile No:</p>{mobileNo}</ListGroup.Item>
                        <ListGroup.Item></ListGroup.Item>
                    </ListGroup>
                </Col>
            </Row>
        </Container>
    );
}
