import { useContext, useEffect, useState } from 'react';
import { Card, Button, Modal } from "react-bootstrap";
import { Link } from 'react-router-dom';
import UserContext from "../UserContext";

export default function ProductView({productProp, index}) { 

    const {user} = useContext(UserContext);

    const {name, description, price, _id} = productProp;    

    const [productName, setProductName] = useState('');
    const [productDescription , setProductDescription] = useState('');
    const [productPrice , setProductPrice] = useState(0);
    const [showConfirm, setShowConfirm] = useState(false);
    const handleCloseConfirm = () => setShowConfirm(false);
    const handleShowConfirm = () => setShowConfirm(true);

    const [showInfo, setShowInfo] = useState(false);
    const handleCloseInfo = () => setShowInfo(false);
    const handleShowInfo = () => setShowInfo(true);

    const placeOrder = (productId) => {
        fetch('https://warm-fjord-87149.herokuapp.com/orders/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: productId,
                quantity: 1,
                totalAmount: price
            })
        })
        .then(response => response.json())
        .then(data => {
            // console.log(data)
            handleCloseConfirm();
            handleShowInfo();
        })
    }

    useEffect(() =>{
        fetch(`https://warm-fjord-87149.herokuapp.com/products/getSingleProduct/${_id}`)
        .then(response => response.json())
        .then(data => {
            // console.log(data);
            setProductName(data.name);
            setProductDescription(data.description);
            setProductPrice(data.price);
        })
    },[_id]);

    return (
        <>
        <Card className='product-card mt-5 border border-dark mx-5' style={{width: "600px"}} bg='light'>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text className='mb-2'>{description}</Card.Text>
                <Card.Text className='mb-3'>PHP {price}</Card.Text>
                { user.id !== null ?
                    <Button variant="warning" onClick={handleShowConfirm}>Order</Button>
                    :
                    <Button variant="outline-dark" as={Link} to={'/login'} className='text-warning fw-bold'>Login</Button>
                    }
            </Card.Body>
        </Card>

        <Modal show={showConfirm} onHide={handleCloseConfirm} backdrop="static" keyboard={false}>
            <Modal.Header closeButton>
                <Modal.Title>Confirm your purchase</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p><strong>Order Details</strong></p>
                <p>{productName} - {productDescription} <br/>Quantity: 1<br/>
                Total Amount: PHP {productPrice} <br/></p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseConfirm}>
                    Cancel
                </Button>
                <Button variant="warning" onClick={() => placeOrder(_id)}>
                    Confirm
                </Button>
            </Modal.Footer>
        </Modal>

        <Modal show={showInfo} onHide={handleCloseConfirm} backdrop="static" keyboard={false}>
            <Modal.Header closeButton>
                <Modal.Title>Order Created</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <h5>Purchase successful!</h5>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="warning" onClick={handleCloseInfo}>
                    Okay
                </Button>
            </Modal.Footer>
        </Modal>
        </>
    )
};



