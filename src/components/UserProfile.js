import { useEffect, useState } from 'react';
import { Container, Row, Col, ListGroup, Alert} from 'react-bootstrap';


export default function UserProfile() {

    const [firstName , setFirstName] = useState('');
    const [lastName , setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo , setMobileNo] = useState('');

    const [totalAmount, setTotalAmount] = useState('');
    const [purchasedOn, setPurchasedOn] = useState('');

    const [productId, setProductId] = useState('');
    const [quantity, setQuantity] = useState('');

    useEffect(() => {
        fetch("https://warm-fjord-87149.herokuapp.com/users/details", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (response => response.json())
        .then(data => {
            // console.log(data._id);
            setFirstName(data.firstName);
            setLastName(data.lastName);
            setEmail(data.email);
            setMobileNo(data.mobileNo);
        })
    }, []);


    useEffect(() => {
        fetch("https://warm-fjord-87149.herokuapp.com/users/viewOrders", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (response => response.json())
        .then(data => {

            data.orders.map((order) => {

                setTotalAmount(order.totalAmount);
                setPurchasedOn(order.purchasedOn);

                order.products.map((product) => {
                    setProductId(product._id);
                    setQuantity(product.quantity);
                    return product;
                })
                return order;
            })
            
        })
    }, []);

    return (
        
        <Container className="mb-5 mt-5 mx-5 my-5">
            <Row>
                <Col>
                    
                    <ListGroup variant="flush">
                        <h1 className="title-header text-dark">Profile Details</h1>
                            <ListGroup.Item></ListGroup.Item>
                            <ListGroup.Item><p className='fw-bold mb-0'>Name:</p> {firstName} {lastName}</ListGroup.Item>
                            <ListGroup.Item><p className='fw-bold mb-0'>Email:</p> {email}</ListGroup.Item>
                            <ListGroup.Item><p className='fw-bold mb-0'>Mobile No:</p> {mobileNo}</ListGroup.Item>
                            <ListGroup.Item></ListGroup.Item>
                    </ListGroup>
                </Col>
            </Row>
        </Container>
    );
}


//userpofile