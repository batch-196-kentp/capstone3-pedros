import {Navbar, Container, Nav} from 'react-bootstrap';
import {useState, useContext} from 'react';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar(){
    const {user} = useContext(UserContext);

    return(
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" className="navbar mb-0 p-0">
        <Container>
          <Navbar.Brand as={Link} to='/' className='text-warning mx-5 fs-1'>Petro Solx</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="my-5">
            </Nav>
            <Nav>
              <Nav.Link as={Link} to='/'>Home</Nav.Link>
              { (user.isAdmin === true) ?
                <Nav.Link as={Link} to="/admin/dashboard">Panel</Nav.Link> 
              :
                <Nav.Link as={Link} to="/products">Products</Nav.Link>
                                      }    

              {
                (user.id !== null) ?
                <>
                <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
                <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                </>
                :
                <>
                <Nav.Link as={Link} to='/login'>Login</Nav.Link>
                <Nav.Link as={Link} to='/register'>Register</Nav.Link>
              </>
              }
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    )
  };

