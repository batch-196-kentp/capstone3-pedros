import { Container, Row, Col, Card, ListGroup, Alert } from 'react-bootstrap';

export default function UserOrderHistoryList({ historyProp }) {
    return (
        historyProp.orders.map((order) =>
            order.products.map((product) =>
            <Card className="mb-4">
                <Card.Header>Purchased on: { order.purchasedOn } </Card.Header>
                <Card.Body>
                    <blockquote className="blockquote mb-0">
                        <p>{' '}
                            Product Id: { product._id } - Quantity: { product.quantity }
                        {' '}</p>
                        <footer className="blockquote-footer">
                            <h5>Total Amount { order.totalAmount }</h5>
                        </footer>
                    </blockquote>
                </Card.Body>
            </Card>
        ))
    );
}
