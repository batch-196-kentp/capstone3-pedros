import {Carousel, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'
export default function Highlights() {
	return (
		<Carousel fade className="carousel">
		      <Carousel.Item className="carousel-item">
		        <img
		          className="d-block w-100 h-100"
		          src="https://www.guttmanenergy.com/wp-content/uploads/2021/02/fuel-products-banner.jpg"
		          alt="First slide"
		        />
		        <Carousel.Caption className='carousel-caption'>
		          <h1 className='text-warning fw-bold'>Petro Solx</h1>
		          <p className='fw-bold'>We keep you fueled!</p>
		        </Carousel.Caption>
		      </Carousel.Item>
		      <Carousel.Item>
		        <img
		          className="d-block w-100 h-100 carousel-image"
		          src="https://www.guttmanenergy.com/wp-content/uploads/2020/11/transportation-fueling-solutions-banner.jpg"
		          alt="Second slide"
		        />

		        <Carousel.Caption className='carousel-caption'>
		          <h2 className='text-warning fw-bold mb-5'>We Offer High Quality Petro Products!</h2>
		          <Button variant="warning" as= {Link} to='/products' className='text-light'>Order Now!</Button>
		        </Carousel.Caption>
		      </Carousel.Item>
		</Carousel>
	);
};
